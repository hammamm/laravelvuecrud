<?php

namespace App\Http\Controllers;

use App\note;
use Illuminate\Http\Request;

class NoteController extends Controller
{

  public function index()
  {
    return view('note/index');
  }


  public function search(Request $request){
    return response()->json([
      'notes'=>note::where('title','like', '%' . $request->input('title') . '%')->get()
    ]);
  }

  public function allData()
  {
    return response()->json(['notes'=>note::all()]);
  }


  public function store(Request $request)
  {
    $data = $request->validate
    ([
      'title' => 'required|max:255',
    ]);
    $note = tap(new note($data))->save();

    return response()->json(['note'=>$note]);
    // return redirect()->route('note.index');
  }


  public function update(Request $request,note $note)
  {

    $data = $request->validate
    ([
      'title' => 'required|max:255',
    ]);
    $note->title = $request->input('title');
    $note->save();
    return response()->json(['note'=>$note]);

    // return redirect()->route('note.index');
  }


  public function destroy($id)
  {
    $note = note::find($id);
    $note->delete();
    return response()->json(['status'=>'success']);
  }
  public function deleteNote($id)
  {

    $note = note::find($id);
    $note->delete();
    return response()->json(['status'=>'success']);
  }
}
