<?php

    Route::resource('note','NoteController');
    Route::get('allData','NoteController@allData');
    Route::get('note/delete/{note}','NoteController@deleteNote');
    Route::post('note/search','NoteController@search');

 ?>
