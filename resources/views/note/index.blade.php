@extends('layouts.master')
@section('title','Notes')
@section('content')

<div id="form">
    <div class="container">
        <div class="row">
            &nbsp;
            &nbsp;
            &nbsp;
            <label for="title">Title:</label>
            <input type="text" v-model="message" name="title" v-on:keyup.enter="submit()">
        &nbsp;
        &nbsp;
        &nbsp;
            <label for="search">Search:</label>
            <input type="text" v-model="searchtxt" name="search" v-on:keyup="search()">

            </div>
        <table class="table">
            <thead>
                <th>id</th>
                <th>title</th>
                <th>action</th>

            </thead>
            <tbody>
                <tr v-for="(note,id) in data">
                    <td>@{{note.id}}</td>
                    <td v-if="note.edit == false">
                        @{{note.title}}
                    </td>
                    <td v-if="note.edit == true">
                        <input type="text" v-model='note.title' v-on:keyup.enter="update(note.id,id)">
                    </td>
                    <td><button class="btn btn-danger" v-on:click="destroy(note.id,id)">X</button>
                        <button type="button" class="btn btn-info" v-on:click="edit(id)"v-if="note.edit ==false" >Edit</button>
                        <button type="button" class="btn btn-info" v-on:click="copy(id)" >copy</button> </td>
                </tr>
            </tbody>

        </table>
    </div>
</div>
@endsection('content')
@section('script')
@include('scripts.noteIndex')
@endsection('script')
