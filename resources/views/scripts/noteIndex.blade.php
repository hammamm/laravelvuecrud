<script>
    var form = new Vue({
        el: '#form',
        data: {
            searchtxt: '',
            message: '',
            data: []
        },
        created: function() {
            this.data = [];
            this.$http.get('/allData').then(result => {
                for (var i = 0; i < result.body.notes.length; i++) {
                    this.data.push({
                        id: result.body.notes[i].id,
                        title: result.body.notes[i].title,
                        edit: false
                    });
                }
            });
        },
        methods: {
            submit: function() {
                console.log('haaaaamaaaaaaaam');
                this.$http.post('/note', {
                    title: this.message,
                    _token: '{{csrf_token()}}'
                }).then(result => {
                    this.data.push({
                        id: result.body.note.id,
                        title: result.body.note.title,
                        edit: false
                    });
                    this.message = '';
                }, result => {
                    console.log(result);
                });

            },
            destroy: function(id, rowid) {
                console.log('/note/' + id);
                this.$http.get('/note/delete/' + id, {
                    _token: '{{csrf_token()}}'
                }).then(
                    result => {
                        this.data.splice(rowid, 1);

                        console.log('success');
                    },
                    result => {
                        console.log(result);
                    }
                );

            },

            update: function(id, rowid) {

                console.log('/note/' + id);
                this.$http.put('/note/' + id, {
                    title: this.data[rowid].title,
                    _token: '{{csrf_token()}}'
                }).then(
                    result => {
                        this.data[rowid].edit = false;
                        console.log(result);
                    },
                    result => {
                        console.log(result);
                    }
                );
            },
            edit: function(id) {
                this.data[id].edit = true;

            },
            copy: function(id) {

                this.message = this.data[id].title;

            },
            search: function() {

                this.$http.post('/note/search', {
                    _token: '{{csrf_token()}}',
                    'title': this.searchtxt
                }).then(
                    result => {

                        this.data = [];
                        for (var i = 0; i < result.body.notes.length; i++) {
                            this.data.push({
                                id: result.body.notes[i].id,
                                title: result.body.notes[i].title,
                                edit: false
                            });
                        }
                    },
                    result => {
                        console.log(result);
                    }
                );

            }

        }
    })
</script>
