<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    @yield('content')
    <script src="https://unpkg.com/vue@2.5.17/dist/vue.js"></script>
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/vue-resource@1.5.1"></script>
    @yield('script')
</body>

</html>
